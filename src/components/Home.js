import React, { Component } from 'react';
import Header from './Header';

class Home extends Component {
    constructor() {
        super();
        this.state = {
            token: localStorage.getItem('token') || '',
        }
        
        this.onLoginAddToken = this.onLoginAddToken.bind(this);
    }

    componentDidUpdate() {
        localStorage.setItem('token', this.state.token);
    }

    onLoginAddToken(token) {
        this.setState({
          token
        });
        console.log(token);
      }

    render() {
        if(!this.state.token.length) {
            this.props.history.push("/login");
        }
        return <div className="app">
            <Header />
            <div className="container">
                <div className="card mt-4">
                    <div className="card-title">Title</div>
                    <div className="card-header">Header</div>
                    <form className="card-body">Body</form>
                    <div className="card-footer">footer</div>
                </div>
            </div>
        </div>
    }
}

export default Home;