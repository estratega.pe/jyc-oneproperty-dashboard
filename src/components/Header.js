import React from 'react';

const Header = () => {
    /*
    const [value, setValue] = React.useState(
      localStorage.getItem('myValueInLocalStorage') || '',
    );
  
    React.useEffect(() => {
      localStorage.setItem('myValueInLocalStorage', value);
    }, [value]);
  
    const onChange = event => setValue(event.target.value);
    //fixed-top
    */

    return (
        <div className="bg-primary">
            <nav className="navbar navbar-dark bg-primary container">
            <a className="navbar-brand" href="/">JyC - Oneproperty</a>
            <ul className="nav nav-pills nav-fill">
            <li className="nav-item">
                    <a className="nav-link active" href="/site">Site</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link active" href="/site/add">add Site</a>
                </li>
                <li className="nav-item">
                    <a className="nav-link active" href="/logout">Logout</a>
                </li>
            </ul>
        </nav>
        </div>
    );
  };

  export default Header;