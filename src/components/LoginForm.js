import React, { Component } from 'react';

class LoginForm extends Component {
    constructor() {
        super();
        this.state = {
            _username: '',
            _password: '',
            token: localStorage.getItem('token') || '',
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        fetch('http://api.estratega.pe/auth/login', {
            method: 'post',
            body: new FormData(document.getElementById('login-form'))//JSON.stringify(this.state)
        }).then(function(response) {
            return response.json();
        }).then((data) => {
            this.setState({
                _username: '',
                _password: '',
                token: data.token
            });
            localStorage.setItem('token', this.state.token);

            this.props.history.push("/");
            
            //this.props.onLoginAddToken(data.token);
        });
    }

    handleInputChange(e) {
        const {value, name} = e.target;
        //console.log(value, name);
        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row mt-4 justify-content-center">
                    <div className="card">
                        <div className="card-header">
                            Login
                        </div>
                        <form onSubmit={this.handleSubmit} className="card-body" id="login-form">
                            <div className="form-group">
                                <input
                                    type="text"
                                    name="_username"
                                    className="form-control"
                                    value={this.state._username}
                                    onChange={this.handleInputChange}
                                    placeholder="Username"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    type="password"
                                    name="_password"
                                    className="form-control"
                                    value={this.state._password}
                                    onChange={this.handleInputChange}
                                    placeholder="Password"
                                />
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Login
                            </button>
                        </form>
                        <div className="card-footer">
                            No tiene una cuenta <a href="/register">Registrate</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default LoginForm;