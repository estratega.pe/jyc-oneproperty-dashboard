import React, { Component } from 'react';

class Logout extends Component {

    logout() {
        //localStorage.removeItem('token')
        localStorage.clear();
        this.props.history.push('/login')
    }

    render() { 
        return <div>
            {this.logout()}
        </div>
    }
}

export default Logout