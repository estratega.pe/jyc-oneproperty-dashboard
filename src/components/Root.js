import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import Login from './LoginForm';

class Root extends Component {
    render() {
      return (
        <BrowserRouter>
          <div>
            <Redirect
              from="/"
              to="/home" />
            <Switch>
              <Route
                path="/home"
                component={Login} /> 
            </Switch>
          </div>
        </BrowserRouter>
      );
    }
  }
  export default Root;