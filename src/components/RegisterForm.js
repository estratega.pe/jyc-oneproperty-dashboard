import React, { Component } from 'react';

class RegisterForm extends Component {
    constructor () {
        super();
        this.state = {
            _username: '',
            _password: '' ,
            _name: '',
            _email: ''
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        fetch('http://api.estratega.pe/auth/register', {
            method: 'post',
            body: new FormData(document.getElementById('register-form'))
        }).then(function(response) {
            return response.json();
        }).then((data) => {
            this.setState({
                _username: '',
                _password: '',
                _name: '',
                _email: ''
            });
            this.props.history.push("/");
            //this.props.onLoginAddToken(data.token);
        });
    }

    handleInputChange(e) {
        const {value, name} = e.target;
        console.log(value, name);
        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row mt-4 justify-content-center">
                    <div className="card">
                        <div className="card-header">
                            Register
                        </div>
                        <form onSubmit={this.handleSubmit} className="card-body" id="register-form">
                            <div className="form-group">
                                <input
                                    type="text"
                                    name="_username"
                                    className="form-control"
                                    value={this.state._username}
                                    onChange={this.handleInputChange}
                                    placeholder="Username"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    type="password"
                                    name="_password"
                                    className="form-control"
                                    value={this.state._password}
                                    onChange={this.handleInputChange}
                                    placeholder="Password"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    type="text"
                                    name="_name"
                                    className="form-control"
                                    value={this.state._name}
                                    onChange={this.handleInputChange}
                                    placeholder="Name"
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    type="email"
                                    name="_email"
                                    className="form-control"
                                    value={this.state._email}
                                    onChange={this.handleInputChange}
                                    placeholder="E-mail"
                                />
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Register
                            </button>
                        </form>
                        <div className="card-footer">
                            <a href="/login">back login</a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default RegisterForm;