import React, { useState, Component } from 'react';
import Header from './Header';

class AddSite extends Component {
    constructor() {
        super();
        this.state = {
            _username: '',
            _password: '',
            token: localStorage.getItem('token') || '',
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //const [greeting, 
    //const setGreeting = useState(
    //    'title property'
    //);

    //const handleChange = event => setGreeting(event.target.value);

    handleSubmit(e) {
        e.preventDefault();
        fetch('http://api.estratega.pe/api/v1/site', {
            method: 'post',
            body: new FormData(document.getElementById('addSite-form'))//JSON.stringify(this.state)
        }).then(function(response) {
            return response.json();
        }).then((data) => {
            this.setState({
                _username: '',
                _password: '',
                token: data.token
            });

            localStorage.setItem('token', this.state.token);

            this.props.history.push("/");
        });
    }

    handleInputChange(e) {
        const {value, name} = e.target;
        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <div className="app">
                <Header />
                <div className="container">
                    <div className="card mt-4">
                        <div className="card-title pl-4">Add new site</div>
                        <div className="card-header">----</div>
                        <form className="card-body" id="addSite-form">
                        <div className="form-group">
                            <input
                                type="text"
                                name="name"
                                className="form-control"
                                placeholder="Username"
                            />
                        </div>
                        <div className="card-body">
                            <label htmlFor="description">Description</label>
                            <textarea className="form-control" name="description"></textarea>
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                name="address"
                                className="form-control"
                                placeholder="Address"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                name="city"
                                className="form-control"
                                placeholder="City"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                name="state"
                                className="form-control"
                                placeholder="state"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                name="country"
                                className="form-control"
                                placeholder="country"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                name="postal_code"
                                className="form-control"
                                placeholder="Postal code"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="number"
                                name="bedroom"
                                className="form-control"
                                placeholder="Bedroom"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="number"
                                name="bathroom"
                                className="form-control"
                                placeholder="bathroom"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="number"
                                name="halfbath"
                                className="form-control"
                                placeholder="halfbath"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="text"
                                name="analytics"
                                className="form-control"
                                placeholder="analytics"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="number"
                                name="gen_list_price"
                                className="form-control"
                                placeholder="150000"
                            />
                        </div>
                        <div className="form-group">
                            <input
                                type="number"
                                name="gen_display_price"
                                className="form-control"
                                placeholder="200000"
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="gen_status_id">Gen status</label>
                            <select
                                name="gen_status_id"
                                className="form-control"
                            >

                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="template_id">Template</label>
                            <select
                                name="template_id"
                                className="form-control"
                            >

                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="status_id">Status</label>
                            <select
                                name="status_id"
                                className="form-control"
                            >

                            </select>
                        </div>
                        </form>
                        <div className="card-footer">
                        <button type="submit" className="btn btn-primary">
                            Create
                        </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default AddSite;

/*
const AddSite = () => {
  const [greeting, setGreeting] = useState(
    'title property'
  );

  const handleChange = event => setGreeting(event.target.value);

  return (
    <Headline headline={greeting} onChangeHeadline={handleChange} />
  );
};

const Headline = ({
  headline,
  onChangeHeadline,
}: {
  headline: string,
  onChangeHeadline: Function,
}) => (
    <div className="app">
        <Header />
        <div className="container">
            <div className="card mt-4">
                <div className="card-title pl-4">Add new site</div>
                <div className="card-header">{headline}</div>
                <form className="card-body">
                  <div className="form-group">
                      <input
                          type="text"
                          name="name"
                          className="form-control"
                          value={headline}
                          onChange={onChangeHeadline}
                          placeholder="Username"
                      />
                  </div>
                  <div className="card-body">
                      <label htmlFor="description">Description</label>
                      <textarea className="form-control" name="description"></textarea>
                  </div>
                  <div className="form-group">
                      <input
                          type="text"
                          name="address"
                          className="form-control"
                          placeholder="Address"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="text"
                          name="city"
                          className="form-control"
                          placeholder="City"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="text"
                          name="state"
                          className="form-control"
                          placeholder="state"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="text"
                          name="country"
                          className="form-control"
                          placeholder="country"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="text"
                          name="postal_code"
                          className="form-control"
                          placeholder="Postal code"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="number"
                          name="bedroom"
                          className="form-control"
                          placeholder="Bedroom"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="number"
                          name="bathroom"
                          className="form-control"
                          placeholder="bathroom"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="number"
                          name="halfbath"
                          className="form-control"
                          placeholder="halfbath"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="text"
                          name="analytics"
                          className="form-control"
                          placeholder="analytics"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="number"
                          name="gen_list_price"
                          className="form-control"
                          placeholder="150000"
                      />
                  </div>
                  <div className="form-group">
                      <input
                          type="number"
                          name="gen_display_price"
                          className="form-control"
                          placeholder="200000"
                      />
                  </div>
                  <div className="form-group">
                      <label htmlFor="gen_status_id">Gen status</label>
                      <select
                          name="gen_status_id"
                          className="form-control"
                      >

                      </select>
                  </div>
                  <div className="form-group">
                      <label htmlFor="template_id">Template</label>
                      <select
                          name="template_id"
                          className="form-control"
                      >

                      </select>
                  </div>
                  <div className="form-group">
                      <label htmlFor="status_id">Status</label>
                      <select
                          name="status_id"
                          className="form-control"
                      >

                      </select>
                  </div>
                </form>
                <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                    Create
                </button>
                </div>
            </div>
        </div>
    </div>
);

export default AddSite;
*/