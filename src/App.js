import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './components/Home';
import LoginForm from './components/LoginForm';
import RegisterForm from './components/RegisterForm';
import Logout from './components/Logout';
import AddSite from './components/AddSite';

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path='/' exact component={Home}/>
          <Route path='/login' exact component={LoginForm}/>
          <Route path='/register' exact component={RegisterForm} />
          <Route path='/site/add' exact component={AddSite} />

          <Route path='/logout' component={Logout}/>
        </Switch>
      </BrowserRouter>
    );
  }
}

/*import React, { Component } from 'react';

import LoginForm from './components/LoginForm';
import Root from './components/Root';

class App extends Component {
  constructor() {
    super();
    this.state = {
      token : ""
    }
    this.onLoginAddToken = this.onLoginAddToken.bind(this);
  }

  onLoginAddToken(token) {
    this.setState({
      token
    });
  }

  render() {
    let content = "";

    if(this.state.token.length) {
      content = "logueado"
    } else {
      content = <LoginForm onLoginAddToken={this.onLoginAddToken}></LoginForm>
    }
    return <div className="container"><Root />
      <div className="row mt-4">
      {content}
      </div>
    </div>
  }
}

export default App;*/